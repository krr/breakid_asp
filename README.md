# Experiment repository for BreakID: Static Symmetry Breaking for ASP #

* The experiments were run with [Benchie](https://bitbucket.org/krr/benchie) in combination with [IDP](dtai.cs.kuleuven.be/software/idp)
* Detailed experimental results are available in the spreadsheet "summary.ods"
* Raw experimental results are available in the folder "[benchdata](https://bitbucket.org/krr/breakid_asp/src/b5d01ac2bad0e4a664b90d6a62fe13071264ef16/benchdata/?at=master)"
* Instances and ASP models reside in the folder "[data](https://bitbucket.org/krr/breakid_asp/src/b5d01ac2bad0e4a664b90d6a62fe13071264ef16/data/?at=master)"
* Binary executables can be found in the folder "[binaries](https://bitbucket.org/krr/breakid_asp/src/b5d01ac2bad0e4a664b90d6a62fe13071264ef16/binaries/?at=master)"